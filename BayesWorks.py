# -*- coding: utf-8 -*-
"""
Created on Sat Nov  5 00:14:37 2022

@author: Jon Wirtzfeld
"""
from logger import get_logger
import pandas as pd
import copy


class BN:
    def __init__(self, *args):
        self.dag = {}
        self.datatable = pd.DataFrame()  # User imported data
        self.nst = pd.DataFrame() # Node State Table
        self.logger = get_logger()
        self.logger.info('New BayesNet Instance')

    # ------------------------------------------------------------------------
    # PUBLIC METHODS
    # ------------------------------------------------------------------------

    # execute
    def execute(self):
        if not self.__isDAG():
            self.logger.critical('Cyclic Graph! BayesWorks will not continue.')
            return None, None
        self.testSequence()
        return self.calculateProbabilities()

    # Calls each node test for each datatable row
    def nodeTests(self):
        self.logger.info('--- Executing Node Tests ---')
        self.__newNST()
        for row in self.datatable.index:
            for node in self.dag:
                rowData = self.datatable.iloc[row]
                self.nst.loc[row, node.name] = node.run(rowData)
        self.logger.info('Done executing node tests!' )

    # Calculate marginal, joint, and conditional probabilities for a select
    # node for a select number of factors
    def probabilities(self, node, factors):
        if(factors):
            factorNames = [factor.name for factor in factors]
            factorStates = [self.nst[factor.name] for factor in factors]
            j = pd.crosstab(index=factorStates,
                            columns=[self.nst[node.name]],
                            rownames=factorNames,
                            colnames=[node.name],
                            dropna=False,
                            margins=True,
                            normalize='all'
                            )
            c = pd.crosstab(index=factorStates,
                            columns=[self.nst[node.name]],
                            rownames=factorNames,
                            colnames=[node.name],
                            dropna=False,
                            margins=True,
                            normalize='columns'
                            )
            return j.reset_index(), c.reset_index()
        return None, None
    
    # Returns complete tables containing marginal, joint and conditional
    # probabilities. Joint/Conditionals broken up by each of the nodes ancestor
    # states. This can lead to large tables!
    def calculateFullProbabilities(self, *args):
        self.logger.info('--- Calculating Node Probabilities ---')
        jointProbabilities = {}
        conditionalProbabilities = {}
        for node in self.dag:
            self.logger.debug('Calculating {} Probabilities'.format(node.name))
            self.__setNodeAncestors(node)
            jointProbabilities[node], conditionalProbabilities[node] = \
            self.probabilities(node, node.ancestors)
        self.logger.info('Done calculating node probabilities!')
        return jointProbabilities, conditionalProbabilities

    # ------------------------------------------------------------------------
    # PRIVATE METHODS
    # ------------------------------------------------------------------------

    # Test to ensure Bayes Net is a DAG
    def __isDAG(self):
        cdag = copy.deepcopy(self.dag)
        parentless = self.__getParentless(cdag)
        while len(parentless) > 0:
            for parentlessNode in parentless:
                for node in cdag:
                    if parentlessNode in cdag[node]:
                        cdag[node].remove(parentlessNode)
            parentless = self.__getParentless(cdag)
        if not cdag:
            return True
        else:
            return False

    def __getParentless(self, dag):
        parentlessNodes = []
        for node in dag:
            if not dag[node]:
                parentlessNodes.append(node)
        for node in parentlessNodes:
            del dag[node]
        return parentlessNodes

    # Creates empty columns named after the BN nodes
    def __newNST(self):
        for node in self.dag:
            self.nst[node.name] = []

    # Sets result from recusive function to ancestors member for node
    def __setNodeAncestors(self, node, parents=None):
        if parents is None:
            parents = []
        node.ancestors = self.__getNodeAncestorsRecursive(node)

    # Get list of parents, grandparents, ect for a specific node
    def __getNodeAncestorsRecursive(self, node, parents=None):
        if parents is None:
            parents = []
        nodeParents = self.dag[node]
        if len(nodeParents) > 0:
            parents.extend(nodeParents)
            for parent in nodeParents:
                self.__getNodeAncestorsRecursive(parent, parents)
        return list(set(parents))


class Node:
    def __init__(self, name, function, possibleStates=[], *args):
        self.name = name
        self.function = function
        self.possibleStates = possibleStates
        self.ancestors = {}

    def run(self, *args):
        return self.function(*args)