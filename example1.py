# -*- coding: utf-8 -*-
"""
Created on Sat Nov  5 16:21:20 2022

@author: Jon Wirtzfeld
"""
import BayesWorks as bw
import random

class PersonalBN(bw.BN):
    def __init__(self):
        super().__init__()
        
        # Rawdata table
        self.datatable["A"] = [1,0,1,1,1,0,1,0,1,0,0,1,0,1,1]
        self.datatable["B"] = [0,0,1,1,2,2,1,0,2,2,0,1,0,2,1]
        self.datatable["C"] = [0,0,0,1,1,1,0,0,0,1,1,1,0,0,0]
        self.datatable["D"] = [0,0,0,1,1,1,0,0,0,1,1,1,0,0,0]
        self.datatable["E"] = [0,0,0,1,1,1,0,0,0,1,1,1,0,0,0]
        
        # Node declarations
        self.A = bw.Node("ANode", ATest)
        self.B = bw.Node("BNode", BTest)
        self.C = bw.Node("CNode", CTest)
        self.D = bw.Node("DNode", DTest)
        self.E = bw.Node("ENode", ETest)
        
        #Dag declaration
        self.dag = {
                self.A : [],
                self.B : [self.A, self.C],
                self.C : [self.A, self.C],
                self.D : [self.B, self.C]
            }

# Nodes are not methods of Child class                
def ATest(row, *args):
    return row["A"]

def BTest(row, *args):
    if row["B"]==0:
        return "B1"
    if row["B"]==1:
        return "B2"
    if row["B"]==2:
        return "B3"
    
def CTest(row, *args):
    return row["C"]

def DTest(row, *args):
        return row["D"]

def ETest(row, *args):
        return random.randint(0,1)
    
    
hi = PersonalBN()
joint, cond = hi.execute()

# print("\n\n\n")
# print(joint)
# print(cond)