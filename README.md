# BayesWorks

WIP: 
BayesWorks (BayesKit?) is a python tool designed to assist in the creation and
analysis of Bayesian Networks. It takes care of the overhead involved with writing
Bayes Net code, allowing users to go from design to implementation in a 
minimal amount of time. 

Using BayesWorks should be as easy as defining your nodes, node tests, and dag
within a child of the BN class and running a couple of the BN class methods. 
The methods output a set of tables containing the marginal probability for each
node state, and the joint/conditional probabilities of each state for 
each node and its parents. 

## TODO:
- **Tests/Logging:** User input verification is important (Check to ensure DAG is
acyclic)
- **Analysis Tools:** Bayes Nets are for making comparisons across time (Contingency 
tables, plots, excel workbook output, etc.)
- **Verify Probabilities:** Not sure if current code is doing things correctly/efficiently
- **User defined Node Combinations:** Users may not want conditionals/joints 
involving every node. Allow them to select a specific set for analysis. 
- **Example:** Create practical example using BayesWorks with real-world data

## Long-term TODO:
- **GUI:** For visualizing DAG and changes in node probabilities
- **BN Training:** Write Algo for training BN